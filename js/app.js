/*

    Notation type "fonction"

*/

function Animal(n){

    //-- Paramètres

    this.nom = n;

    //-- Méthodes

    this.toHTML = function(){
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(this.nom);
        node.appendChild(textnode);
        document.getElementById("bestiaire").appendChild(node);
    }

    //-- Autres actions réalisées à la construction (constructeur)

    this.toHTML();
}



/*

    Variante avec prototype

* /

function Animal(n){
    this.nom = n;
    this.toHTML();
}

Animal.prototype.toHTML = function(){
    var node = document.createElement("DIV");
    var textnode = document.createTextNode(this.nom);
    node.appendChild(textnode);
    document.getElementById("bestiaire").appendChild(node);  
}




/*

    Notation JSON

    ATTENTION : pour cette notation, remplacez dans le code
    new Animal("Flipper") par Animal.init("Flipper")
* /

var Animal = {
    nom: "",
    toHTML: function(){
        var node = document.createElement("DIV");
        var textnode = document.createTextNode(this.nom);
        node.appendChild(textnode);
        document.getElementById("bestiaire").appendChild(node);  
    },

    init: function(n){
        this.nom = n;
        this.toHTML();

        return this;
    }
}


/*  */

